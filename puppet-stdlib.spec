Name:           puppet-stdlib
Version:        2.2
Release:        1%{?dist}
Summary:        Puppetlabs stdlib module

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz
BuildArch:      noarch
Requires:       puppet-agent

%description
Puppetlabs stdlib module.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/stdlib/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/stdlib/
touch %{buildroot}/%{_datadir}/puppet/modules/stdlib/supporting_module

%files -n puppet-stdlib
%{_datadir}/puppet/modules/stdlib
%doc code/README.md

%changelog
* Wed Oct 04 2023 Ben Morrice <ben.morrice@cern.ch> 2.2-1
- Rebase to v9.3.0

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.1-3
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.1-2
- fix requires on puppet-agent

* Wed Jan 08 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- Rebase to v5.2.0

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebuild for 7.5 release
- Rebase to newer puppet module

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
-Initial release

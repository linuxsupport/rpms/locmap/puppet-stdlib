# frozen_string_literal: true

# @summary DEPRECATED.  Use the namespaced function [`stdlib::fqdn_rotate`](#stdlibfqdn_rotate) instead.
Puppet::Functions.create_function(:fqdn_rotate) do
  dispatch :deprecation_gen do
    repeated_param 'Any', :args
  end
  def deprecation_gen(*args)
    call_function('deprecation', 'fqdn_rotate', 'This method is deprecated, please use stdlib::fqdn_rotate instead.')
    call_function('stdlib::fqdn_rotate', *args)
  end
end
